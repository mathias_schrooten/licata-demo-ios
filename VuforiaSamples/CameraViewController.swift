//
//  CameraViewController.swift
//  MaesContainers
//
//  Created by Jonas Beckers on 27/03/18.
//  Copyright © 2018 Appwise. All rights reserved.
//

import AVFoundation
import Foundation
import UIKit

@objc public protocol CameraViewControllerDelegate: AnyObject {
    func cameraViewController(didFocus point: CGPoint)
    func cameraViewController(update status: AVAuthorizationStatus)
    func cameraViewController(captured image: UIImage)
}

@available(iOS 10.2, *)
@objc open class CameraViewController: UIViewController {
    @IBOutlet var containerView: UIView!
    public var isConfiguring: Bool = false

    public var preset: AVCaptureSession.Preset = .hd4K3840x2160
    public var videoGravity: AVLayerVideoGravity = .resizeAspectFill
    public var lowLightBoost: Bool = false

    public var tapToFocus: Bool = false
    public var flashMode: AVCaptureDevice.FlashMode = .off

    public var cameraPosition: AVCaptureDevice.Position = .back {
        didSet {
            reconfigureSession()
        }
    }

    public var dataCaptureEnabled: Bool = true

    private var queue = DispatchQueue(label: "com.jonasbeckers.camera")
    @objc public var session: AVCaptureSession = AVCaptureSession()
    private(set) var previewLayer: AVCaptureVideoPreviewLayer?

    private var captureDevice: AVCaptureDevice?
    private var captureDeviceInput: AVCaptureDeviceInput?
    private var capturePhotoOutput: AVCapturePhotoOutput?
    private var captureVideoOutput: AVCaptureVideoDataOutput?

    @objc public weak var cameraDelegate: CameraViewControllerDelegate?

    var videoSize: CMVideoDimensions {
        guard let description = captureDeviceInput?.device.activeFormat.formatDescription else { return CMVideoDimensions(width: 0, height: 0) }
        return CMVideoFormatDescriptionGetDimensions(description)
    }

    override open func viewDidLoad() {
        super.viewDidLoad()

        let previewLayer = AVCaptureVideoPreviewLayer(session: session)
        previewLayer.videoGravity = videoGravity
        previewLayer.connection?.videoOrientation = .portrait
        containerView.layer.insertSublayer(previewLayer, at: 0)
        self.previewLayer = previewLayer

        let status = AVCaptureDevice.authorizationStatus(for: .video)
        switch status {
        case .authorized:
            configureSession()
            cameraDelegate?.cameraViewController(update: status)
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video) { [unowned self] granted in
                let newStatus = AVCaptureDevice.authorizationStatus(for: .video)
                if granted {
                    self.configureSession()
                }
                self.cameraDelegate?.cameraViewController(update: newStatus)
            }
        default:
            cameraDelegate?.cameraViewController(update: status)
        }
    }

    override open func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)

        queue.async {
            guard self.session.isRunning else { return }
            self.session.stopRunning()
        }
    }

    override open func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        queue.async {
            guard !self.session.isRunning else { return }
            self.session.startRunning()
        }
    }

    override open func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        previewLayer?.frame = containerView.bounds
        previewLayer?.videoGravity = videoGravity
        previewLayer?.connection?.videoOrientation = .portrait
    }

    // make async
    @objc public func takePhoto() {

        guard let output = capturePhotoOutput else { return }

        if (!session.isRunning && !isConfiguring) {
            session.startRunning()
        }

        if((captureDevice?.isFocusModeSupported(.continuousAutoFocus))!) {
            try! captureDevice?.lockForConfiguration()
            captureDevice?.focusMode = .continuousAutoFocus
            captureDevice?.unlockForConfiguration()
        }

        let settings = AVCapturePhotoSettings()
        settings.flashMode = flashMode
        settings.isHighResolutionPhotoEnabled = true
        settings.isAutoStillImageStabilizationEnabled = true

        let previewPixelType = settings.availablePreviewPhotoPixelFormatTypes.first ?? 0
        let previewFormat = [
            kCVPixelBufferPixelFormatTypeKey as String: previewPixelType,
            kCVPixelBufferWidthKey as String: 160,
            kCVPixelBufferHeightKey as String: 160
        ]



        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(200)) {
            output.capturePhoto(with: settings, delegate: self)
        }
    }

   override open func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard tapToFocus, let touch = touches.first else { return }

        let location = touch.preciseLocation(in: view)
        let size = view.bounds.size
        let focusPoint = CGPoint(x: location.x / size.height, y: 1 - location.x / size.width)

        guard let captureDevice = captureDevice else { return }
        do {
            try captureDevice.lockForConfiguration()
            if captureDevice.isFocusPointOfInterestSupported {
                captureDevice.focusPointOfInterest = focusPoint
                captureDevice.focusMode = .autoFocus
            }
            if captureDevice.isExposurePointOfInterestSupported {
                captureDevice.exposurePointOfInterest = focusPoint
                captureDevice.exposureMode = .continuousAutoExposure
            }
            captureDevice.unlockForConfiguration()
            cameraDelegate?.cameraViewController(didFocus: location)
        } catch {
            print(error)
        }
    }
}

@available(iOS 10.2, *)
extension CameraViewController {
    func reconfigureSession() {
        session.inputs.forEach { session.removeInput($0) }
        captureDeviceInput = nil
        captureDevice = nil

        session.outputs.forEach { session.removeOutput($0) }
        captureVideoOutput = nil
        capturePhotoOutput = nil

        configureSession()
    }

    @objc public func configureSession() {
        isConfiguring = true
        queue.async {
            self.session.beginConfiguration()

            if self.session.canSetSessionPreset(self.preset) {
                self.session.sessionPreset = self.preset
            } else {
                self.session.sessionPreset = .high
            }

            self.configureCaptureDevice()
            self.configureCaptureDeviceInput()
            if #available(iOS 11.0, *) {
                self.configureCapturePhotoOutput()
            }
            self.configureCaptureVideoOutput()

            self.session.commitConfiguration()
            self.isConfiguring = false
        }
    }

    private func configureCaptureDevice() {
        let device = captureDevice(for: cameraPosition)
        guard let captureDevice = device else { return }

        do {
            try captureDevice.lockForConfiguration()

            if captureDevice.isFocusModeSupported(.continuousAutoFocus) {
                captureDevice.focusMode = .continuousAutoFocus
            }

            if captureDevice.isSmoothAutoFocusSupported {
                captureDevice.isSmoothAutoFocusEnabled = true
            }

            if captureDevice.isExposureModeSupported(.continuousAutoExposure) {
                captureDevice.exposureMode = .continuousAutoExposure
            }

            if captureDevice.isWhiteBalanceModeSupported(.continuousAutoWhiteBalance) {
                captureDevice.whiteBalanceMode = .continuousAutoWhiteBalance
            }

            if captureDevice.isLowLightBoostSupported && lowLightBoost {
                captureDevice.automaticallyEnablesLowLightBoostWhenAvailable = true
            }

            captureDevice.unlockForConfiguration()
        } catch {
            print(error)
        }

        self.captureDevice = captureDevice
    }

    private func configureCaptureDeviceInput() {
        do {
            guard let captureDevice = captureDevice else { return }
            let captureDeviceInput = try AVCaptureDeviceInput(device: captureDevice)

            if session.canAddInput(captureDeviceInput) {
                session.addInput(captureDeviceInput)
            }

            self.captureDeviceInput = captureDeviceInput
        } catch {
            print(error)
        }
    }

    private func configureCaptureVideoOutput() {
        let captureVideoOutput = AVCaptureVideoDataOutput()
        captureVideoOutput.alwaysDiscardsLateVideoFrames = true
        captureVideoOutput.setSampleBufferDelegate(self, queue: DispatchQueue(label: "CameraViewControllerQueue"))

        if session.canAddOutput(captureVideoOutput) {
            session.addOutput(captureVideoOutput)
        }

        self.captureVideoOutput = captureVideoOutput
    }

    @available(iOS 11.0, *)
    private func configureCapturePhotoOutput() {
        let capturePhotoOutput = AVCapturePhotoOutput()
        capturePhotoOutput.isHighResolutionCaptureEnabled = true

        if capturePhotoOutput.isDualCameraDualPhotoDeliverySupported {
            capturePhotoOutput.isDualCameraDualPhotoDeliveryEnabled = true
        }

        if session.canAddOutput(capturePhotoOutput) {
            session.addOutput(capturePhotoOutput)
        }

        self.capturePhotoOutput = capturePhotoOutput
    }

    private func captureDevice(for position: AVCaptureDevice.Position) -> AVCaptureDevice? {
        let session = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInWideAngleCamera], mediaType: AVMediaType.video, position: position)
        let devices = session.devices
        let wideAngle = devices.first { $0.position == position }
        return wideAngle
    }
}

@available(iOS 10.2, *)
extension CameraViewController: AVCaptureVideoDataOutputSampleBufferDelegate {
    public func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {

    }
}

@available(iOS 10.2, *)
extension CameraViewController: AVCapturePhotoCaptureDelegate {
    @available(iOS 11.0, *)
    public func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        DispatchQueue.global(qos: .userInitiated).async {
            guard let data = photo.fileDataRepresentation(), let image = UIImage(data: data) else { return }
            DispatchQueue.main.async { [weak self] in
                self?.cameraDelegate?.cameraViewController(captured: image)
            }
        }
    }
}
